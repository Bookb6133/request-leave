@extends('layouts.app')

@section('content')

    <div class="container">
        <h2 align="center"> Request </h2>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Manager Status</th>
                    <th scope="col">HR Status</th>
                    <th scope="col">Nickname</th>
                    <th scope="col">Leave Type</th>
                    <th scope="col">Start leaving</th>
                    <th scope="col">Return at</th>
                    <th scope="col">Total day(s)</th>
                    <th scope="col">Reason to leave</th>
                    <th scope="col">Pending Responsibility</th>          
                </tr>
            </thead>
            <tbody>
            {{-- to show data, will use @foreach loop --}}
                @foreach ($data as $row) {{-- เอาขอมูลมาจากตัวแปรอาเรย์ $data ในController แล้วก็ให้โชว์as $row โชว์ทีละตัว--}}
                    <tr>
                        @if ($row->status_manager == "rejected")
                            <td class="table-danger">{{ $row->status_manager }}</td>
                        @elseif ($row->status_manager == "approved")
                            <td class="table-success">{{ $row->status_manager }}</td>
                        @else
                            <td class="table-light">{{ $row->status_manager }}</td>
                        @endif
                        @if ($row->status_hr == "rejected")
                            <td class="table-danger">{{ $row->status_hr }}</td>
                        @elseif ($row->status_hr == "approved")
                            <td class="table-success">{{ $row->status_hr }}</td>
                        @else
                            <td class="table-light">{{ $row->status_hr }}</td>
                        @endif
                            <td>{{ $row->user->nickname }}</td>
                            <td>{{ $row->type->name }}</td>
                            <td>{{ date("d-m-Y", strtotime($row->from_date)) }}</td>
                            <td>{{ date("d-m-Y", strtotime($row->to_date)) }}</td>
                            <td>{{ $row->total_days }}</td>
                            <td>{{ $row->reason }}</td>
                            <td>{{ $row->pending_responsibilities }}</td>
                                                      
                    </tr>
                @endforeach
                   
            </tbody>
        </table>
    </div>

@endsection