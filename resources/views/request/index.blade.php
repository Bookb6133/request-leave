@extends('layouts.app')

@section('content')

    <div class="container">
        <h2 align="center"> Request </h2>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Request date</th>
                    <th scope="col">Nickname</th>
                    <th scope="col">Department</th>
                    <th scope="col">Leave Type</th>
                    <th scope="col">Start</th>
                    <th scope="col">Return</th>
                    <th scope="col">Total day(s)</th>
                    <th scope="col">Manager Approval</th>
                    <th scope="col">HR Approval</th>
                    <th scope="col"></th>
                
                </tr>
            </thead>
            <tbody>
            {{-- to show data, will use @foreach loop --}}
                @foreach ($requests as $row) {{-- เอาขอมูลมาจากตัวแปรอาเรย์ $data ในController แล้วก็ให้โชว์as $row โชว์ทีละตัว--}}
                    
                        <tr>
                            <td scope="row">{{ $loop->iteration }}</td>
                            <td>{!! date("d-m-Y H:i:s", strtotime($row->created_at)) !!}</td>
                            <td>{{ $row->user->nickname }}</td>
                            <td>{{ $row->user->department }}</td>
                            <td>{{ $row->type->name }}</td>
                            <td>{{ date("d-m-Y", strtotime($row->from_date)) }}</td>
                            <td>{{ date("d-m-Y", strtotime($row->to_date)) }}</td>
                            <td>{{ $row->total_days }}</td>
                             @if ($row->status_manager == "rejected")
                                <td class="table-danger">{{ $row->status_manager }}</td>
                            @elseif ($row->status_manager == "approved")
                                <td class="table-success">{{ $row->status_manager }}</td>
                            @else
                                <td class="table-light">{{ $row->status_manager }}</td>
                            @endif
                            @if ($row->status_hr == "rejected")
                                <td class="table-danger">{{ $row->status_hr }}</td>
                            @elseif ($row->status_hr == "approved")
                                <td class="table-success">{{ $row->status_hr }}</td>
                            @else
                                <td class="table-light">{{ $row->status_hr }}</td>
                            @endif

                            <td><a class="btn btn-outline-info" href="{{ route('request.edit', $row->id) }}">Permission</a></td>
                            <td>
                                <form action="{{ route('request.destroy', $row->id) }}" method="POST">
                                    @csrf @method('DELETE')
                                    <input type="submit" value="Delete" data-name="{{ $row->user->nickname }}" class="btn btn-outline-danger deleteForm"
                                    onclick="return confirm('Do you want to delete {{$row->user->nickname}} ?')">
                                </form>
                            </td>

                        </tr>
                    
                @endforeach
                   
            </tbody>
        </table>
    </div>

@endsection