@extends('layouts.app')

@section('content')
{{-- Laravel Collective --}}
<div class="container">
    <div class="row justify-content-center">
    {{-- Error messages when missing fields --}}
    @if ($errors->all())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    <div class="col-lg-6">
    {{-- A Request Form --}}
    {{-- open a form --}}
    {{ Form::open(['route' => 'request.store', 'method' => 'POST'],) }}
        {{-- <div class="row"> --}}
        

            <div class="form-group">
                {!! Form::label('which is your leave type?') !!}
                {!! Form::select('type_id', ['' => '--Select--'] + $leave_type, null, ['class' => 'form-control form-type']) !!} 
            </div>
            {{-- [] + [] = one [] --}}

            <div class="form-group">
                {!! Form::label('Start from ?') !!}
                {!! Form::date('from_date', \Carbon\Carbon::now(), ['class' => 'form-control start-leave-date']) !!} 
            </div>
            <div class="form-group">
                {!! Form::checkbox('half_day[]', 'morning leave') !!} 
                {!! Form::label('morning leave', 'morning leave', ['class' => 'text-lowercase']) !!}
            </div>
            <div class="form-group">
                {!! Form::checkbox('half_day[]', 'afternoon leave') !!} 
                {!! Form::label('afternoon leave', 'afternoon leave', ['class' => 'text-lowercase']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('Returning at ?') !!}
                {!! Form::date('to_date', \Carbon\Carbon::now(), ['class' => 'form-control end-leave-date']) !!} 
            </div>
            <div class="form-group">
                {!! Form::checkbox('half_day[]', 'return in the afternoon') !!} 
                {!! Form::label('return in the afternoon', 'return in the afternoon', ['class' => 'text-lowercase']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Why you ask for leaving?') !!}
                {!! Form::text('reason', " ",['class' => 'form-control reason-to-leave']) !!} 
            </div>

            <div class="form-group">
                {!! Form::label('What is your pending responsibility?') !!}
                {!! Form::text('pending_responsibilities', "",['class' => 'form-control pending-responsibilities']) !!} 
            </div>

            <div class="form-group">
                {!! Form::submit('SUBMIT') !!}
            </div>

        </div>
        </div>
    </div>
</div>
@endsection