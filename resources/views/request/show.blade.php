@extends('layouts.app');
@section ('content');

    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width:28rem;">
                <div class="card-body">
                    <h3 class="card-title text-center">{{ $data->user->firstname }} {{ $data->user->surname }}</h5>
                        <ul class="list-group">
                            <li class="list-group-item">Request for: <span>{{ $data->type->name }}</span></li>
                            <li class="list-group-item">Strat date: <span>{{ date("d-m-Y", strtotime($data->from_date)) }}</span></li>
                            <li class="list-group-item">Return date: <span>{{ date("d-m-Y", strtotime($data->to_date)) }}</span></li>
                            <li class="list-group-item">Total: <span>{{ $data->total_days}} day(s)</span></li>
                            <li class="list-group-item">Reason to leave: <span>{{ $data->reason}}</span></li>
                            <li class="list-group-item">Pending Responsibility: <span>{{ $data->pending_responsibilities}}</span></li>
                            <li class="list-group-item">Status (manager, hr): <span>{{ $data->status_manager . ", " . $data->status_hr }}</span></li>
                        </ul>
                    
                        
                </div>
            </div>
        </div>
    </div>

@endsection