@extends('layouts.app');
@section ('content');

    <div class="container">
        <div class="row justify-content-center">
            <div class="card" style="width:28rem;">
                <div class="card-body">
                    <h3 class="card-title text-center">{{ $data->user->firstname }} {{ $data->user->surname }}</h5>
                        <ul class="list-group">
                            <li class="list-group-item">Request for: <span>{{ $data->type->name }}</span></li>
                            <li class="list-group-item">Strat date: <span>{{ date("d-m-Y", strtotime($data->from_date)) }}</span></li>
                            <li class="list-group-item">Return date: <span>{{ date("d-m-Y", strtotime($data->to_date)) }}</span></li>
                            <li class="list-group-item">Total: <span>{{ $data->total_days}} day(s)</span></li>
                            <li class="list-group-item">Reason to leave: <span>{{ $data->reason}}</span></li>
                            <li class="list-group-item">Pending Responsibility: <span>{{ $data->pending_responsibilities}}</span></li>
                            
                            @can('can-approve', $data)
                            <li class="list-group-item">
                                @switch ($user_role)
                                    @case ($user_role->isAdmin())
                                        {!! Form::open(['action' => ['RequestLeaveController@update', $data->id], 'method' => 'PUT']) !!}
                                            {{-- Approval from team's manager --}}
                                            {!! Form::label('status_manager', 'Status (from manager): ') !!}
                                            {!! Form::select('status_manager', ['pending' => 'Pending', 'approved' => 'Approved', 'rejected' => 'Rejected'], $data->status_manager) !!}                                        
                                            <br>{{-- Approval from HR --}}
                                            {!! Form::label('status_hr', 'Status (from HR): ') !!}
                                            {!! Form::select('status_hr', ['pending' => 'Pending', 'approved' => 'Approved', 'rejected' => 'Rejected'], $data->status_hr) !!}                                    
                                            <br>
                                            {!! Form::submit('Save') !!}
                                        {!! Form::close() !!}
                                    @break
                                    @case ($user_role->isHr())
                                        {!! Form::open(['action' => ['RequestLeaveController@update', $data->id], 'method' => 'PUT']) !!}
                                            {!! Form::label('status_hr', 'Status (from HR): ') !!}
                                            {!! Form::select('status_hr', ['pending' => 'Pending', 'approved' => 'Approved', 'rejected' => 'Rejected'], $data->status_hr) !!}                                       
                                            {!! Form::submit('Save') !!}
                                        {!! Form::close() !!}
                                    @break
                                    @case ($user_role->isManager())
                                        {!! Form::open(['action' => ['RequestLeaveController@update', $data->id], 'method' => 'PUT']) !!}
                                            {!! Form::label('status_manager', 'Status (from manager): ') !!}
                                            {!! Form::select('status_manager', ['pending' => 'Pending', 'approved' => 'Approved', 'rejected' => 'Rejected'], $data->status_manager) !!}                                       
                                            {!! Form::submit('Save') !!}
                                        {!! Form::close() !!}
                                    @break
                                @endswitch
                            </li>
                            @endcan
                        </ul>

                </div>
            </div>
        </div>
    </div>

@endsection