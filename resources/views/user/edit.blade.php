@extends('layouts.app')

@section('content')
    {{-- will apply Laravel Collective for this create Form -> https://laravelcollective.com/docs/6.x/html --}}

    <div class="container">
        <div class="row justify-content-center">
            {{-- Error messages --}}
            @if ($errors->all())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error )
                        <li>
                            {{$error}}
                        </li>            
                    @endforeach
                </ul>
            @endif
            <div class="col-xs-12 col-sm-6 col-md-6">
                {!! Form::open(['action' => ['UserController@update', $data->id], 'method'=>'PUT']) !!} {{--After submit where the action will take--}}
                    <div class="form-group">
                        {!! Form::label('First Name') !!}
                        {!! Form::text('firstname', $data->firstname, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Last Name') !!}
                        {!! Form::text('surname', $data->surname, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Fullname in Thai') !!}
                        {!! Form::text('thai_name', $data->thai_name, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Start Working at') !!}
                        {!! Form::date('first_day_of_work', $data->first_day_of_work, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('NICKNAME') !!}
                        {!! Form::text('nickname', $data->nickname, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('Department') !!}
                        {!! Form::select('department', ['' => '--Select--'] + \App\User::departments, $data->department, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>
                @can ('can-update-role', $data)
                    <div class="form-group">
                        {!! Form::label('User Role') !!}
                        {!! Form::select('user_role', \App\User::user_roles, $data->user_role, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>
                @endcan    
                    <div class="form-group">
                        {!! Form::label('Email Adress') !!}
                        {!! Form::email('email', $data->email, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Line ID') !!}
                        {!! Form::email('line_id', $data->line_id, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label('Phone') !!}
                        {!! Form::number('phone_number', $data->phone_number, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Birthday') !!}
                        {!! Form::date('date_of_birth', $data->date_of_birth, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Emergency Contact Name') !!}
                        {!! Form::email('emergency_name', $data->emergency_name, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Emergency Contact Number') !!}
                        {!! Form::email('emergency_number', $data->emergency_number, ['class' => 'form-control']) !!} {{-- Form text (this input name, default value, class) --}}
                    </div>

                    {{ Form::submit('UPDATE') }}
                    <a href="/user" class="btn">BACK</a>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection