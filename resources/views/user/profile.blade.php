@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                {{-- <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}

            <div class="container bootstrap snippets bootdey">
                <div class="panel-body inf-content">
                    <div class="row">
                        
                        <div class="col-md-8 pt-4">
                            <div class="text-center font-weight-bold">{{ $user->nickname }}'s Information</div><br>
                                <div class="table-responsive">
                                    <table class="table table-user-information">
                                        <tbody>
                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">First Name</span></td>
                                                <td class="text-sm-left">{{$user->firstname}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Last Name</span></td>
                                                <td class="text-sm-left">{{$user->surname}}</td>
                                            </tr>

                                            <tr>
                                                <td><span class="font-weight-bold text-primary">First day at Lexicon</span></td>
                                                <td class="text-sm-left">{{$user->first_day_of_work}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Fullname (Thai)</span></td>
                                                <td class="text-sm-left">{{$user->thai_name}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Nickname</span></td>
                                                <td class="text-sm-left">{{$user->nickname}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Date of Birth</span></td>
                                                <td class="text-sm-left">{{$user->date_of_birth}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Mobile Phone</span></td>
                                                <td class="text-sm-left">{{$user->phone_number}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Email Address</span></td>
                                                <td class="text-sm-left">{{$user->email}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Line ID</span></td>
                                                <td class="text-sm-left">{{$user->line_id}}</td>
                                            </tr>

                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Department</span></td>
                                                <td class="text-sm-left">{{ ucfirst(str_replace('-', ' ', $user->department)) }}</td>
                                            </tr>
                                            
                                            <tr>        
                                                <td><span class="font-weight-bold text-primary">Emergency Contact</span></td>
                                                <td class="text-sm-left">{{$user->emergency_name}}</td>
                                                <td class="text-sm-left">{{$user->emergency_number}}</td>
                                            </tr>


                                        </tbody>
                                    </table>
                                </div> {{-- table-responsive --}}
                        </div> {{--col-md-8 pt-4--}}
                        
                        <div class="col-md-4 py-3">
                            @switch ($user)
                                    @case ($user->isAdmin())
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('user.edit', $user->id) }}">Edit Profile</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('list-request') }}"> My Requests</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('request.create') }}"> Request Leave</a>
                                        <a type="button" class="mt-3 btn btn-outline-info btn-sm btn-block" href="{{ route('user.index') }}">All Users</a>
                                        <a type="button" class="mt-3 btn btn-outline-info btn-sm btn-block" href="{{ route('requests.index') }}">All Requests</a>
                                        <a type="button" class="mt-3 btn btn-outline-success btn-sm btn-block" href="{{ route('leave-type') }}">All Leave Types</a>
                                        {{-- <a type="button" class="mt-3 btn btn-outline-success btn-sm btn-block" href="{{ route('create-leave-type') }}">Create a Leave Type</a> --}}
                                    @break

                                    @case ($user->isHr())
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('user.edit', $user->id) }}">Edit Profile</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('list-request') }}"> My Requests</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('request.create') }}"> Request Leave</a>
                                        <a type="button" class="mt-3 btn btn-outline-info btn-sm btn-block" href="{{ route('requests.index') }}">All Requests</a>

                                    @break

                                    @case ($user->isManager())
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('user.edit', $user->id) }}">Edit Profile</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('list-request') }}"> My Requests</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('request.create') }}"> Request Leave</a>
                                        <a type="button" class="mt-3 btn btn-outline-info btn-sm btn-block" href="{{ route('team-request') }}"> Team Requests</a>
                                        
                                    @break

                                    @case ($user->isUser())
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('user.edit', $user->id) }}">Edit Profile</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('list-request') }}"> My Requests</a>
                                        <a type="button" class="mt-3 btn btn-outline-secondary btn-sm btn-block" href="{{ route('request.create') }}"> Request Leave</a>
                                    @break
                                @endswitch
                                                       
                        </div> {{--col-md-4 pt-3--}}
                    
                </div>

                {{-- balance table --}}
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Type</th>
                        <th scope="col" class="text-center">Used</th>
                        <th scope="col" class="text-center">Available</th>
                        <th scope="col" class="text-center">Allowance</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($types as $type)
                    <tr>
                        <th scope="row">{{$type->name}}</th>
                        <th scope="row" class="text-center">{{$type->used_days}}</th>
                        <th scope="row" class="text-center">{{$type->allowance -$type->used_days}}</th>
                        <th scope="row" class="text-center">{{$type->allowance}}</th>
                    </tr>
                    @empty
                    <tr>
                        <th scope="row" colspan="4" class="text-center">Nothing</th>
                    </tr>
                    @endforelse
            </div>
            </div> {{-- container bootstrap snippets bootdey--}}
        </div>
        </div>
    </div>
</div>
@endsection
