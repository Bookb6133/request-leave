@extends('layouts.app')

@section('content')
    @can('can-see-all-users', $data)
    <div class="container">
        <h2 align="center"> Lexicon's Users </h2>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Thai Name</th>
                    <th scope="col">Nickname</th>
                    <th scope="col">Profile</th>
                    <th scope="col">Actions</th>
                    
                </tr>
            </thead>
            <tbody>
            {{-- to show data, will use @foreach loop --}}
                @foreach ($data as $row) {{-- เอาขอมูลมาจากตัวแปรอาเรย์ $data ในController แล้วก็ให้โชว์as $row โชว์ทีละตัว--}}
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                            <td>{{  $row->firstname }}</td>
                            <td>{{ $row->surname }}</td>
                            <td>{{ $row->thai_name }}</td>
                            <td>{{ $row->nickname }}</td> 
                            <td>
                                <a href="{{ route('user.show', $row->id) }}" class="btn btn-outline-secondary">Profile</a>
                            </td>
                            {{-- edit --}}
                            <td>
                                <a href="{{ route('user.edit', $row->id)}} " class="btn btn-outline-secondary">Edit</a>
                            </td>
                            {{-- delete --}}
                            <td>
                                <form action="{{ route('user.destroy', $row->id) }}" method="POST">
                                    @csrf @method('DELETE')
                                    <input type="submit" value="Delete" data-name="{{ $row->firstname }}" class="btn btn-outline-danger deleteForm"
                                    onclick="return confirm('Do you want to delete {{$row->firstname}} ?')">
                                </form>
                            </td>
                            
                    </tr>
                @endforeach
                   
            </tbody>
        </table>
    </div>
    @endcan
@endsection