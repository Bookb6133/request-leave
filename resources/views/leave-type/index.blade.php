@extends('layouts.app');
@section ('content');

    <div class="container">
        <div>
            <a type="button" class="mb-3 btn btn-secondary btn-sm" href="{{ route('create-leave-type') }}">Create a new Leave Type</a>
        </div>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th style="width: 10%" class="text-center"></th>
                    <th style="width: 30%" class="text-center">Type Name</th>
                    <th style="width: 50%" class="text-center">Allowance (days)</th>
                    <th style="width: 5%" class="text-center"></th>
                    <th style="width: 5%" class="text-center"></th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($leavetype as $type)
                <tr>
                    <th class="text-center">{{ $loop->iteration }}</th>
                    <td class="text-center">{{ $type->name }}</td>
                    <td class="text-center">{{ $type->allowance }}</td>
                    {{-- edit --}}
                    <td>
                        <a href="{{ route('edit-leave-type', $type->id)}} " class="btn btn-outline-secondary">Edit</a>
                    </td>
                    {{-- delete --}}
                    <td>
                        <form action="{{ route('delete-leave-type', $type->id) }}" method="POST">
                            @csrf @method('DELETE')
                            <input type="submit" value="Delete" data-name="{{ $type->name }}" class="btn btn-outline-secondary deleteForm"
                            onclick="return confirm('Do you want to delete {{$type->name}} ?')">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
            
        </table>
    </div>
 

@endsection