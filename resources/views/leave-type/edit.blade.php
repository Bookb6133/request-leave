@extends('layouts.app')

@section('content')
{{-- Laravel Collective --}}
<div class="container">
    <div class="justify-content-center">

    {{ Form::open(['route' => ['update-leave-type', $type->id], 'method' => 'PUT']) }}

        <div class="form-group">
            {!! Form::label('Type Name') !!}
            {!! Form::text('name', $type->name, ['class' => 'form-control form-type']) !!} 
        </div>
        <div class="form-group">
            {!! Form::label('The number of days employees can take leave per year') !!}
            {!! Form::text('allowance', $type->allowance, ['class' => 'form-control from-type']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Update') !!}
        </div>
    {{ Form::close() }}



    </div>
</div>
@endsection