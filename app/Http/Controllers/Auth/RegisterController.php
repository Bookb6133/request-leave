<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:20',
            'surname' => 'max:20',  //check at users table
            'nickname' => 'max:20',
            'thai_name' => 'max:50',
            'first_day_of_work' => 'required|date',
            'department' => 'required|in:' . implode(',', array_keys(User::departments)), //implode = convert string to array
            'phone_number' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'date_of_birth' => 'date',
            'email' => 'required|email|max:100|unique:users',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'surname' => $data['surname'],
            'nickname' => $data['nickname'],
            'thai_name' => $data['thai_name'],
            'department' => $data['department'],
            'first_day_of_work' => $data['first_day_of_work'],
            'phone_number' => $data['phone_number'],
            'date_of_birth' => $data['date_of_birth'],
            'email' => $data['email'],
            'line_id' => $data['line_id'],
            'emergency_name' => $data['emergency_name'],
            'emergency_number' => $data['emergency_number'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
