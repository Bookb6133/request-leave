<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveType;

class LeaveTypeController extends Controller
{
    /** To view all types of leave */
    public function index() {

        $this->authorize('viewAny', LeaveType::class);
        $leavetype = LeaveType::all();
        
        return view('leave-type.index', compact(['leavetype']));
    }

    /** To create a new type of leave*/
    public function create() {
        return view('leave-type.create');
    }

    /** To store a new type of leave*/
    public function store(Request $request) {

        //validate input
        $request -> validate([
            'name'      => 'required',
            'allowance' => 'required'
        ]);
        
        $leaveType = new LeaveType;

        $leaveType->name        =   $request->name;
        $leaveType->allowance   =   $request->allowance;
        
        // var_dump($request->all());
        $leaveType->save();

        //flash messages tell user that the request succeeded
        session()->flash('success', $request->name . " " . 'has been created successfully');

        return redirect(route('leave-type'));
    }

    /** To edit a new type of leave*/
    public function edit($id) {
        $type = LeaveType::findOrFail($id);
        return view('leave-type.edit', compact(['type']));
    }

    /** To edit a new type of leave*/
    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $request -> validate([
            'name'      => 'required',
            'allowance' => 'required'
        ]);
        // var_dump($request->id);
        $type = LeaveType::where('id', $id)->first();

        $type->name = $request->input('name');
        $type->allowance = $request->input('allowance');
        
        $type->save();

        //flash messages tell user that the request updated
        session()->flash('success', $request->name . " " . 'has been updated successfully');

        return redirect(route('leave-type'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $leavType = LeaveType::find($id);
        $leavType->delete();

        //flash messages tell user that the request succeeded
        session()->flash('delete', $leavType->name . " " . 'has been deleted');

        return redirect(route('leave-type'));
    }

}
