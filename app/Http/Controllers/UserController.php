<?php

namespace App\Http\Controllers;

use App\LeaveType;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\RequestLeave;
use Illuminate\Support\Carbon;

class UserController extends Controller
{

    /**
     * Display a listing of the team's users.
     * only team's manager can see this list.
     *
     * @return \Illuminate\Http\Response
     */
    public function teamIndex()
    {
        //get current user (manager)
        $current_department = Auth::user()->department;
        //need to use data where manager department = user's department
        $data = User::where('department', $current_department)->get();
        // $this->authorize('viewTeam', $data);
        // var_dump($data);
        return view('user.team-index', compact(['data', 'current_department']));
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //need to use data from user model and save them to $data
        $data = User::orderBy('firstname', 'asc')->orderBy('department', 'asc')->get();
        // print_r($data);
        // return json_decode($data);
            return view('user.index', compact(['data']));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|max:20',
            'surname' => 'required|max:20',  //check at users table
            'nickname' => 'required|max:20',
            'first_day_of_work' => 'required|date',
            'department' => 'required|in:' . implode(',', array_keys(User::departments)),
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'date_of_birth' => 'required|date',
            'email' => 'required|email|max:100|unique:users',
        ]);
        
        //save data through User Model
        User::create($request -> all());
            return redirect(route('profile'));

    }

    /**
     * Display the profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $current_user = Auth::user()->id;
        // var_dump($current_user);
        //using query in show method.

        
        return $this->show($current_user);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = User::findOrFail($id);  //parameter that is not existing will lead to 404.
        $this->authorize('view', $profile);

        $now = Carbon::now();
        $startDate = Carbon::parse($profile->first_day_of_work);
        // x = different years btw now and first_day_of_work
        $x = $now->diffInYears($startDate);
        // $x will be used to find the year that the staff is working.
        // so, $x + 1 will be the next working year
        $anniversary = $startDate->addYears($x);
        // dd($anniversary);
        
        //select * from leave_types table
        //$types = LeaveType::all()
        //select request_leaves join id = type_id Sun total_days
        $types = $profile->requestLeaves()
            ->join('leave_types', 'type_id', '=', 'leave_types.id')
            ->selectRaw('SUM(total_days) as used_days') //Aggregation
            ->whereBetween('from_date', [$anniversary, $now]) //btw now and the anniversary first day of work
            ->addSelect('leave_types.name', 'allowance')
            ->groupBy('leave_types.name', 'allowance') 
            ->get();
            // dd($types);
        return view('user.profile', [
            'user' => $profile,
            'types' => $types
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get the details from User($id)
        $data = User::find($id);
        $this->authorize('update', $data);
        return view('user.edit', compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'firstname' => 'required|max:20',
            'surname' => 'required|max:20',
            'nickname' => 'required|max:20',
            'first_day_of_work' => 'required|date',
            'department' => 'required|in:' . implode(',', array_keys(User::departments)),
            'user_role' => 'in:admin,manager,user,hr',
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'date_of_birth' => 'required|date',
            'email' => 'required|email|max:100',
        ]);
        
        // dd($request->all());
        //flash messages tell user that the request succeeded
        session()->flash('success', $request->firstname . " " . 'has been updated successfully');
        //save data through Usre Model
        
        User::find($id)->update($request->all());

        if(Auth::user()->user_role === 'admin') {
            return redirect(route('user.index'));   
        } else {
            return redirect(route('profile'));
        }       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find(1);

        $user->delete();
        //flash messages after deleted
        session()->flash('delete', $user->firstname . " " . 'has been deleted');
    }
}
