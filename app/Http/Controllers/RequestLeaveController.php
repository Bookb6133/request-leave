<?php

namespace App\Http\Controllers;

use App\LeaveType;
use Illuminate\Http\Request;
use App\RequestLeave;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class RequestLeaveController extends Controller
{

     /** Display a listing of logged-in user requests **/
     public function listRequest() {
        //get current user id
        $current_user = Auth::user()->id;
        
        //query request details and keep in $data
        $data = RequestLeave::where('user_id', $current_user)->orderByDesc('created_at')->get();
        // var_dump($data); 
        return view('request.list-request', compact(['data']));
    }

    /**
     * Display a listing of the team's resources.
     * โดยสร้าง]arameter ชื่อว่า $department ขึ้นมา
     *
     * @return \Illuminate\Http\Response
     */
    public function teamRequest($department = null) //{department?}
    {
        //if the server did't receive the department paremeter, 
        //the department will equal to logged-in user's department
        if (!$department) {
            $department = Auth::user()->department;
        }

        //department of the user requested (#Querying Relations)
        //request lists which manager department = department of user who post the request
        //declaring "use ($department)" means that we can use this var inside the query.
        $requests = RequestLeave::whereHas('user', function($q) use ($department) {
            $q->where('department', $department);
        })->orderBy( 'created_at', 'desc')->get();
        // dd($requests->type);
            return view('request.team-request', compact(['requests']));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //need to use data from RequestLeave model and save them to $request
        $requests = RequestLeave::orderByDesc('created_at')->get();
        
            $this->authorize('viewAny', RequestLeave::class);
            return view('request.index', compact(['requests']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leave_type = LeaveType::pluck('name', 'id')->all();
        // foreach ($leave_type as $name => $id)
        // dd($leave_type);
        //create.blade.php
        return view('request.create', compact(['leave_type']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // validate input data
        $request -> validate([
            'type_id' => 'required',
            'from_date' => 'required|date',
            'to_date' => 'required|date|after:yesterday|after_or_equal:from_date',
            'reason' => 'required|max:200',
            'pending_responsibilities' => 'required|max:200',
        ]);
        
        //save input data
            $user = Auth::user()->id;
            // $leave_type = \App\LeaveType::first();
            //$totalDays = RequestLeave::select('DATEDIFF(request_leaves.start_date, request_leaves.end_date as totalDays)
            // dd(Auth::user());
            $userRequest = new RequestLeave;

            $userRequest->type_id       = $request->type_id;
            $userRequest->to_date       = $request->to_date;
            $userRequest->from_date     = $request->from_date;
            //calculate total day Using Carbon class and diffInWeekDays() will only day without weekend 
            $total_days    = Carbon::parse($request->from_date)->diffInWeekDays(Carbon::parse($request->to_date));
            //$userRequest->half_day      ?? implode(', ', $request->half_day);
            $userRequest->half_day = (is_null($userRequest->half_day)) ? $userRequest->total_days = $total_days : implode(', ', $request->half_day);
            switch ( $request->half_day ) {
                case ['morning leave']:
                    $userRequest->total_days = $total_days + 0.5;
                    break;
                case ['afternoon leave']:
                    $userRequest->total_days = $total_days - 0.5;
                    break;
                case ['return in the afternoon']:
                    $userRequest->total_days = $total_days + 0.5;
                    break;   
                case ['afternoon leave', 'return in the afternoon']:
                    $userRequest->total_days = $total_days;
                    break;                  
            }

            $userRequest->reason        = $request->reason;
            $userRequest->pending_responsibilities = $request->pending_responsibilities;
            $userRequest->user_id       = $user;
            // dd($request->all());
            $userRequest->save();

            //flash messages tell user that the request succeeded
            session()->flash('success', 'The request has been created successfully');
            
            return redirect(route('list-request'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = RequestLeave::findOrFail($id);
        $this->authorize('view', $data);
        // var_dump($requests);
            return view('request.show', compact(['data']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = RequestLeave::find($id);
        $user_role = Auth::user();
        
            return view('request.edit', compact(['data', 'user_role']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status_manager' => 'in:pending,approved,rejected',
            'status_hr' => 'in:pending,approved,rejected',
        ]);
        // dd($request);
        //get the first data
        // $this->authorize('approve', RequestLeave::class);
        $requestLeave = RequestLeave::where('id', $id)->first();
        // var_dump($requestLeave);
        
        if(!empty($request->input('status_manager')))
            $requestLeave->status_manager = $request->input('status_manager');
       
        if(!empty($request->input('status_hr')))
            $requestLeave->status_hr = $request->input('status_hr');
        // dd($request->input('status_admin'));
        // $request->input('$key'); will accept only input request.

        $requestLeave->save();
            if(Auth::user()->user_role === 'admin' || Auth::user()->user_role === "hr"){
                return redirect('/requests');   
            } elseif (Auth::user()->user_role === 'manager') {
                return redirect('/team-request');
            }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requestleave = RequestLeave::find($id);
        $requestleave->delete();

        //flash messages after deleted
        session()->flash('delete', 'The request has been deleted');
    }
}
