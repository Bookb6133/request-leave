<?php

namespace App;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveType extends Model
{
    // use HasFactory;

    //hey are not actually removed from your database. Instead, a deleted_at attribute is set.
    use SoftDeletes;
    

    /**
     * The attributes that are mass assignable!!!!!.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'allowance'
    ];

    //Leave type has many request application
    public function requestApplications() {
        return $this->hasMany(RequestLeave::class);
    }
}
