<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\RequestLeavePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\RequestLeave'  => 'App\Policies\RequestLeavePolicy',
        'App\User'          => 'App\Policies\UserPolicy'
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        //User
        Gate::define('can-see-all-users', 'App\Policies\UserPolicy@viewAny');
        Gate::define('can-view-team', 'App\Policies\UserPolicy@viewTeam');
        Gate::define('can-update-role', 'App\Policies\UserPolicy@updateRole');
        
        //RequestLeave
        Gate::define('can-approve', 'App\Policies\RequestLeavePolicy@approve');
        Gate::define('manager-can-approve', 'App\Policies\RequestLeavePolicy@managerApprove');
        Gate::define('can-see-team-request', 'App\Policies\RequestLeavePolicy@viewTeamRequest');

    }
}
