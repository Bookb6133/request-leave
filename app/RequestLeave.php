<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestLeave extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'from_date',
        'to_date', 
        'reason', 
        'pending_responsibilities', 
        'status_manager', 'status_hr', 
        'half_day'
    ];

    protected $casts = [
        'half_day'  => 'array'
    ];

    // Request application has one user.
    public function user() {
        return $this->belongsTo(User::class);
    }

    //Request application has one leave Type
    //get the type that owns the requestleave.
    //default foreign key => relationship method + _id
    public function type() {
        return $this->belongsTo(LeaveType::class);
    }

    /**
     * Mutators
     * jason_encode the half_day conditions
     * returns the JSON representation of a value
     */
    // public function setHalfDayAttribute($value) {
    //     $this->attributes['half_day'] = json_encode($value);
    // }

    /**
     * Accessors
     * jason_decode the half_day conditions
     * encoded string and converts it into a PHP variable
     */
    // public function getHalfDayAttribute($value) {
    //     return $this->attributes['half_day'] = json_decode($value);
    // }

}
