<?php

namespace App\Policies;

use App\RequestLeave;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RequestLeavePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether only the Admin can view any models.
     * Ture or False?
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin() || $user->isHr();
    }

     /**
     * Determine whether the admin and manager can see team's requests.
     * Ture or False?
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function viewTeamRequest(User $user)
    {
        //Admin can see any requests.
        //Managers can see team's requests.
        //Users can see only their requests.
        return $user->isAdmin()
            || $user->isManager();
        
    }

    /**
     * Determine whether the user can view the model.
     * Ture or False?
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function view(User $user, RequestLeave $requestLeave)
    {
        return $user->isAdmin()
            || $user->isHr()
            || ($user->isManager() && $user->department == $requestLeave->user->department)
            || ($user->isUser() && $user->id == $requestLeave->user_id);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function update(User $user, RequestLeave $requestLeave)
    {
        // return $user->isAdmin();
    }

    /**
     * Determine whether the user can approve the request.
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function approve(User $user, RequestLeave $requestLeave)
    {
        //Admin can approve the requests.
        //ANd Manager can arrove the team's requests. (manager, department = user who requested is in the same department.)
        return $user->isAdmin()
            || $user->isHr()
            || ($user->isManager() && $user->department == $requestLeave->user->department);
    }


    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function delete(User $user, RequestLeave $requestLeave)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function restore(User $user, RequestLeave $requestLeave)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\RequestLeave  $requestLeave
     * @return mixed
     */
    public function forceDelete(User $user, RequestLeave $requestLeave)
    {
        //
    }
}
