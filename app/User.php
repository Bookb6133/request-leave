<?php

namespace App;
use Illuminate\Support\Str;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    const user_roles = [
        'admin'     => 'Admin',
        'hr'        => 'Human Resource',
        'manager'   => 'Team Manager',
        'user'      => 'User'
    ];
    
    const departments = [
        'operations'            => 'Operations',
        'strategy-and-ideation' => 'Strategy and Ideation',
        'video-production'      => 'Video Production',
        'social-media'          => 'Social Media',
        'graphic-design'        => 'Graphic Design',
        'tech-lab'              => 'Tech Lab',
        'client-services'       => 'Client Services'
    ];

    use Notifiable;

    /**
     * The attributes that are mass assignable!!!!!.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 
        'surname', 
        'nickname',
        'thai_name',
        'first_day_of_work',
        'position', 
        'department', 
        'phone_number', 
        'date_of_birth', 
        'email', 
        'password',
        'user_role',
        'line_id',
        'emergency_name',
        'emergency_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'first_day_of_word',
        'date_of_birth',
    ];

    // Mutators -> capitalize input data
    public function setFirstnameAttribute($value) {
        $this->attributes['firstname'] = Str::ucfirst($value);
    }

    public function setSurnameAttribute($value) {
        $this->attributes['surname'] = Str::ucfirst($value);
    }

    // User has many requests.
    public function requestleaves() {
        return $this->hasMany(RequestLeave::class);
    }

    public function isAdmin() {
        return $this->user_role === 'admin';
    }

    public function isManager() {
        return $this->user_role === 'manager';
    }

    public function isHr() {
        return $this->user_role === 'hr';
    }

    public function isUser() {
        return $this->user_role === 'user';
    }

}
