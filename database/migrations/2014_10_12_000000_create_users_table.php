<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('user_role')->default('user')->index();
            $table->string('firstname');
            $table->string('surname');
            $table->string('thai_name');
            $table->string('nickname');
            $table->date('first_day_of_work')->nullable();
            $table->string('department')->index();      
            $table->string('phone_number');
            $table->string('line_id');
            $table->string('email')->unique();
            $table->date('date_of_birth');
            $table->string('emergency_name');
            $table->string('emergency_number');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
