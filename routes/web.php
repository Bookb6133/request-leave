<?php

use App\LeaveType;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\LeaveTypeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//****Gruop the Route that need to be log in before access the website****//
Route::middleware(['auth'])->group(function () {

    Route::resource('user', UserController::class);
    //user profile
    Route::get('/', 'UserController@profile')->name('profile');
    //team's user
    Route::get('department', 'UserController@teamIndex')->name('team-index');


    Route::resource('requests', RequestLeaveController::class)->only(['index']);
    Route::resource('profile/request', RequestLeaveController::class)->except(['index']);
    //all requests of the user
    Route::get('profile/list-request', 'RequestLeaveController@listRequest')->name('list-request');
    // {department?} คือเราสร้างพารามิตเตอร์ให้url สามารถรับค่าได้ แล้วก็ ? ที่อยู่ข้างหลังแปลว่า มีหรือไม่มีก็ได้
    //team's requests
    Route::get('team-request/{department?}', 'RequestLeaveController@teamRequest')->name('team-request');


    Route::get('/leave-type', [LeaveTypeController::class, 'index'])->name('leave-type');
    Route::get('/leave-type/create', [LeaveTypeController::class, 'create'])->name('create-leave-type');
    Route::post('/leave-type', [LeaveTypeController::class, 'store']);
    Route::get('leave-type/{id}/edit', [LeaveTypeController::class, 'edit'])->name('edit-leave-type');
    Route::put('/leave-type/{id}', [LeaveTypeController::class, 'update'])->name('update-leave-type');
    Route::delete('/leave-type/{id}', [LeaveTypeController::class, 'destroy'])->name('delete-leave-type');

});


